export class Recipe {
  constructor(
    public id: string,
    public imageUrl: string,
    public name: string,
    public description: string,
    public ingredients: string,
    public steps: string
  ) {}
}
