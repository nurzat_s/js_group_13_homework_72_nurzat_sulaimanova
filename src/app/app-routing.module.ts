import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipesComponent } from './recipes/recipes.component';
import { RecipeResolverService } from './recipes/recipe-resolver.service';
import { HomeComponent } from './home/home.component';
import { NewRecipeComponent } from './new-recipe/new-recipe.component';
import { RecipeDetailsComponent } from './recipe-details/recipe-details.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'new', component: NewRecipeComponent},
  {path: 'view', component: RecipeDetailsComponent},
  {path: 'recipe', component: RecipesComponent, children: [

      {
        path: ':id',
        component: RecipesComponent,
        resolve: {
          recipe: RecipeResolverService
        }
      },
    ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
