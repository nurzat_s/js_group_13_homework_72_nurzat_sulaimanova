import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { RecipeService } from '../shared/recipe.service';
import { Router } from '@angular/router';
import { Recipe } from '../shared/recipe.model';

@Component({
  selector: 'app-new-recipe',
  templateUrl: './new-recipe.component.html',
  styleUrls: ['./new-recipe.component.css']
})

export class NewRecipeComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  recipeForm!: FormGroup;

  isUploading = false;
  recipeUploadingSubscription!: Subscription;

  constructor(private recipeService: RecipeService, private router: Router) { }

  ngOnInit(): void {
    this.recipeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      imageUrl: new FormControl('', Validators.required),
      ingredients: new FormControl('', Validators.required),
      steps: new FormArray([])
    });

    this.recipeUploadingSubscription = this.recipeService.recipeUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    })
  }

  onSubmit() {
    console.log(this.recipeForm);
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.recipeForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  addStep() {
    const steps = <FormArray>this.recipeForm.get('steps');
    const stepGroup = new FormGroup({
      image: new FormControl('', Validators.required),
      description: new FormControl('')
    })
    steps.push(stepGroup);
  }

  getStepControls() {
    const steps =  <FormArray>this.recipeForm.get('steps');
    return steps.controls;
  }

  saveRecipe() {
    const id = Math.random().toString();

    const recipe = new Recipe(
      id,
      this.form.value.imageUrl,
      this.form.value.name,
      this.form.value.description,
      this.form.value.ingredients,
      this.form.value.steps
    );


    const next = () => {
      void this.router.navigate(['/']);
    };

    this.recipeService.addRecipe(recipe).subscribe(next);

  }

  ngOnDestroy() {
    this.recipeUploadingSubscription.unsubscribe();
  }

}
