import { Component, OnDestroy, OnInit } from '@angular/core';
import { Recipe } from '../shared/recipe.model';
import { Subscription } from 'rxjs';
import { RecipeService } from '../shared/recipe.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})

export class RecipesComponent implements OnInit, OnDestroy {
  recipes!: Recipe[];
  recipesChangeSubscription!: Subscription;
  recipesFetchingSubscription!: Subscription;
  isFetching: boolean = false;

  constructor(private recipeService: RecipeService) { }

  ngOnInit(): void {
    this.recipes = this.recipeService.getRecipes();
    this.recipesChangeSubscription = this.recipeService.recipesChange.subscribe((recipes: Recipe[]) => {
      this.recipes = recipes;
    })
    this.recipesFetchingSubscription = this.recipeService.recipesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.recipeService.fetchRecipes();
  }

  ngOnDestroy() {
    this.recipesChangeSubscription.unsubscribe();
    this.recipesFetchingSubscription.unsubscribe();
  }

}
