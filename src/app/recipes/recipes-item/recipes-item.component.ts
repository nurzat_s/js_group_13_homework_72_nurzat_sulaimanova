import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from '../../shared/recipe.model';
import { RecipeService } from '../../shared/recipe.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute,  Router } from '@angular/router';

@Component({
  selector: 'app-recipes-item',
  templateUrl: './recipes-item.component.html',
  styleUrls: ['./recipes-item.component.css']
})

export class RecipesItemComponent implements OnInit {
  @Input() recipe!: Recipe;
  isRemoving = false;
  recipeRemovingSubscription!: Subscription;

  constructor(private recipeService: RecipeService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    // this.route.data.subscribe((data: Data) => {
    //   const recipe = <Recipe>data['recipe'];
    //   this.recipe = recipe;
    // });
    this.recipeRemovingSubscription = this.recipeService.recipeRemoving.subscribe((isRemoving: boolean) => {
      this.isRemoving = isRemoving;
    });
  }

  onRemove() {
    this.recipeService.removeRecipe(this.recipe.id).subscribe(() => {
      this.recipeService.fetchRecipes();
      void this.router.navigate(['..'], {relativeTo: this.route});
    });
  }

}
